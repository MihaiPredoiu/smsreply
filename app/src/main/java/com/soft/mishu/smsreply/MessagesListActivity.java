package com.soft.mishu.smsreply;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

public class MessagesListActivity extends AppCompatActivity {

    private final DBHandler dbHandler = new DBHandler(this,null);;
    private final int ACTIONS_MENU = R.id.actions_menu;
    private EntriesAdapter myAdapter;
    private ArrayList<Integer> selectedItems = new ArrayList<>();
    private ListView listView;
    private Menu menu;
    private MenuItem edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages_list);

        listView = findViewById(R.id.list);

        myAdapter = new EntriesAdapter(this, dbHandler.getMessagesList());
        listView.setAdapter(myAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_messages_list, menu);

        menu.setGroupVisible(ACTIONS_MENU, false);
        this.menu = menu;
        edit = menu.findItem(R.id.action_edit);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MessagesListActivity.this);
        switch (item.getItemId()) {
            case R.id.action_delete:
                String nrOfMessages =
                        selectedItems.size() == 1 ? "one message" : Integer.toString(selectedItems.size()) + " messages";

                builder.setMessage("Are you sure you want to delete " + nrOfMessages + "?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dbHandler.deleteMessages(selectedItems);

                                refreshListView();
                                removeSelection();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing
                            }
                        })
                        .show();
                return true;

            case R.id.action_edit:
                View newView = LayoutInflater.from(MessagesListActivity.this).inflate(R.layout.view_add_message, null);
                final EditText editText = newView.findViewById(R.id.editText);
                final TextView textView = newView.findViewById(R.id.textView);
                final int itemId = selectedItems.get(0);

                textView.setText(getString(R.string.edit_message));
                editText.setText(dbHandler.getMessageById(itemId));
                editText.setSelection(editText.getText().length());

                builder.setView(newView)
                        .setCancelable(true)
                        .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dbHandler.updateMessage(itemId, editText.getText().toString());
                                refreshListView();
                                removeSelection();
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                removeSelection();
                            }
                        })
                        .show();

                return true;

            case R.id.action_cancel:
                removeSelection();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (selectedItems.isEmpty()) {
            super.onBackPressed();
        } else {
            removeSelection();
        }
    }

    // OnClick action for floating button
    public void addMessage(View view) {
        View newView = LayoutInflater.from(MessagesListActivity.this).inflate(R.layout.view_add_message, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(MessagesListActivity.this);
        final EditText editText = newView.findViewById(R.id.editText);

        removeSelection();

        builder.setView(newView)
                .setCancelable(true)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Message message = new Message(editText.getText().toString());
                        dbHandler.addMessage(message);
                        refreshListView();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        removeSelection();
                    }
                })
                .show();
    }

    public class EntriesAdapter extends ArrayAdapter<Message> {
        public EntriesAdapter(Context context, ArrayList<Message> entries) {
            super(context, 0, entries);
        }

        @Override
        public View getView(int pos, View view, ViewGroup parent) {
            final Message message = getItem(pos);
            final int position = pos;

            view = getLayoutInflater().inflate(R.layout.view_message, null);

            final Switch switchObj = view.findViewById(R.id.switch1);
            final TextView textView = view.findViewById(R.id.textView);

            textView.setText(message.getMessage());
            textView.setId(message.getId());
            switchObj.setChecked(message.isActive());
            switchObj.setId(message.getId());

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (!selectedItems.contains(textView.getId())) {
                        selectedItems.add(textView.getId());
                        if (selectedItems.isEmpty()) {
                            menu.setGroupVisible(ACTIONS_MENU, false);
                        } else {
                            menu.setGroupVisible(ACTIONS_MENU, true);
                            if (selectedItems.size() > 1) {
                                edit.setVisible(false);
                            }
                        }
                        view.setBackgroundColor(getResources().getColor(R.color.colorAccentLight));
                    }
                    return true;
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!selectedItems.isEmpty()) {
                        if (selectedItems.contains(textView.getId())) {
                            selectedItems.remove(new Integer(textView.getId()));
                            view.setBackgroundColor(android.R.attr.selectableItemBackground);
                        } else {
                            selectedItems.add(textView.getId());
                            view.setBackgroundColor(getResources().getColor(R.color.colorAccentLight));
                        }
                        if (selectedItems.isEmpty()) {
                            menu.setGroupVisible(ACTIONS_MENU, false);
                        } else {
                            menu.setGroupVisible(ACTIONS_MENU, true);
                            if (selectedItems.size() > 1) {
                                edit.setVisible(false);
                            }
                        }
                    }
                }
            });


            switchObj.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    dbHandler.toggleMessage(buttonView.getId(), isChecked);
                    refreshListView();
                }
            });
            return view;
        }
    }

    private void refreshListView() {
        myAdapter.clear();
        myAdapter.addAll(dbHandler.getMessagesList());
        myAdapter.notifyDataSetChanged();
    }

    private void removeSelection() {
        selectedItems.clear();
        menu.setGroupVisible(ACTIONS_MENU, false);
        refreshListView();
    }
}
