package com.soft.mishu.smsreply;

public class Message {
    private int id;
    private String message;
    private boolean active;

    public Message(int id, String message, boolean active) {
        this.id = id;
        this.message = message;
        this.active = active;
    }

    public Message(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void toggleActive() {
        active = !active;
    }
}
