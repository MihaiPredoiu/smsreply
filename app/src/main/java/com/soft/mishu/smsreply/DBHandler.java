package com.soft.mishu.smsreply;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DBHandler  extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "sms.db";
    private static final String TABLE_MESSAGES = "messages";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_MESSAGE = "message";
    private static final String COLUMN_ACTIVE = "active";

    public DBHandler(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query;
        query = "CREATE TABLE " + TABLE_MESSAGES + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_ACTIVE + " BOOLEAN DEFAULT 0, " +
                COLUMN_MESSAGE + " TEXT NOT NULL " +
                ");";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES);
        onCreate(db);
    }

    public void addMessage(Message message) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_MESSAGE, message.getMessage());
        db.insert(TABLE_MESSAGES, null, values);
        db.close();
    }

    public void deleteMessages(ArrayList<Integer> messages) {
        SQLiteDatabase db = getWritableDatabase();
        String query;

        for (int i = 0; i < messages.size(); i++) {
            query = "DELETE FROM " + TABLE_MESSAGES +
                    " WHERE " + COLUMN_ID + " = " + Integer.toString(messages.get(i));

            db.execSQL(query);
        }
    }

    public String getMessageById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        String message = "";
        String query = "SELECT " + COLUMN_MESSAGE +
                " FROM " + TABLE_MESSAGES +
                " WHERE " + COLUMN_ID + " = " + Integer.toString(id);

        Cursor cursor = db.rawQuery(query, null);
        try {
            cursor.moveToFirst();
            message = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE));
        } catch (Exception e) {
            Log.d("DEBUG", e.getMessage());
        }

        return message;
    }

    public String getActiveMessage() {
        SQLiteDatabase db = getWritableDatabase();
        String message;
        String query = "SELECT " + COLUMN_MESSAGE +
                " FROM " + TABLE_MESSAGES +
                " WHERE " + COLUMN_ACTIVE + " = 1";
        try {
            Cursor c = db.rawQuery(query, null);
            c.moveToFirst();
            message = c.getString(c.getColumnIndex(COLUMN_MESSAGE));
        } catch (Exception e){
            message = "";
        }

        db.close();
        return message;
    }

    public void updateMessage(int id, String message) {
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE " + TABLE_MESSAGES +
                " SET " + COLUMN_MESSAGE + " = '" + message + "'" +
                " WHERE " + COLUMN_ID + " = " + Integer.toString(id);
        db.execSQL(query);
    }

    public void toggleMessage(int id, boolean isChecked) {
        SQLiteDatabase db = getWritableDatabase();
        String query;

        if (isChecked) {
            query = "UPDATE " + TABLE_MESSAGES +
                    " SET " + COLUMN_ACTIVE + " = 0" +
                    " WHERE " + COLUMN_ACTIVE + " = 1";
            db.execSQL(query);

            query = "UPDATE " + TABLE_MESSAGES +
                    " SET " + COLUMN_ACTIVE + " = 1" +
                    " WHERE " + COLUMN_ID + " = " + Integer.toString(id);
            db.execSQL(query);
        } else {
            query = "UPDATE " + TABLE_MESSAGES +
                    " SET " + COLUMN_ACTIVE + " = 0" +
                    " WHERE " + COLUMN_ID + " = " + Integer.toString(id);
            db.execSQL(query);
        }
    }

    public ArrayList<Message> getMessagesList() {
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT " + COLUMN_ID + ", " + COLUMN_MESSAGE + ", " + COLUMN_ACTIVE +
                " FROM " + TABLE_MESSAGES +
                " ORDER BY " + COLUMN_ID + " ASC";
        ArrayList<Message> list = new ArrayList<>();

        Cursor cursor = db.rawQuery(query, null);
        try {
            cursor.moveToFirst();
            do {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
                String message = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE));
                boolean active = cursor.getInt(cursor.getColumnIndex(COLUMN_ACTIVE)) > 0;

                list.add(new Message(id, message, active));
            } while(cursor.moveToNext());
        } catch (Exception e) {
            Log.d("DEBUG", e.getMessage());
        }

        return list;
    }
}
