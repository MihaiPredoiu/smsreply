package com.soft.mishu.smsreply;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;

public class CallListener extends BroadcastReceiver {
    private static boolean isRinging = false;
    private static boolean isReceived = false;
    private String phoneNumber;
    private String state;

    public void onReceive(Context context, Intent intent) {
        // Get the current phone state
        state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

        if (state == null) {
            return;
        }

        Bundle bundle = intent.getExtras();

        // Phone is ringing
        if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            Log.d("DEBUG", "RINGING ");
            isRinging = true;
            phoneNumber = bundle.getString("incoming_number");
        }

        // Incoming call is received
        if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
            isRinging = false;
            isReceived = true;
        }

        // Phone is idle
        if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
            // Missed call
            if(isRinging && !isReceived) {
                DBHandler dbHandler = new DBHandler(context, null);

                try {
                    String message = dbHandler.getActiveMessage();
                    Log.d("DEBUG", "IDLE " + message);
                    if (message != "") {
                        SmsManager sms = SmsManager.getDefault();
                        sms.sendTextMessage(phoneNumber, null, message, null, null);
                    }
                } catch (Exception e) {
                }

                isRinging = false;
                Toast.makeText(context, "Missed call: " + phoneNumber, Toast.LENGTH_LONG).show();
                Log.d("DEBUG", "MISSED CALL " + phoneNumber);
            }
            isReceived = false;
        }}
}
